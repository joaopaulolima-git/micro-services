package com.jpdevhrpayroll.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpdevhrpayroll.entities.Payment;
import com.jpdevhrpayroll.entities.Worker;
import com.jpdevhrpayroll.feignclients.WorkerFeignClient;

@Service
public class PaymentService {
	
		
	@Autowired
	private WorkerFeignClient workerFeignClient;
	
	public Payment getPayment(long workerId , int days) {
			
		Worker worker = workerFeignClient.findById(workerId).getBody();
		return new Payment(worker.getName(), worker.getDailyIncome(), days);
		}
}
