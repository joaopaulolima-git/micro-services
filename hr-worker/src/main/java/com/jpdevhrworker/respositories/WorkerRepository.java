package com.jpdevhrworker.respositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpdevhrworker.entities.Worker;

public interface WorkerRepository extends JpaRepository<Worker, Long> {

}
